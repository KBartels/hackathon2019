<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function getSensorData() {
        $sensorData = DB::table('sensor')
            ->orderBy('id', 'desc')
            ->limit(1)
            ->get();
        return view('home', ['sensorData' => $sensorData]);
    }
}
