@extends('layouts.app')

@section('content')
    <div class="main-container">
        <div class="Date">
            <p>{{ date('l, d.m.Y H:m') }}</p>
        </div>
        <div class="overlay">
            <div class="col-md-4 temperature">
                @foreach($sensorData as $data)
                    <p><i class="icon-thermometer"></i>
                        {{ $data->temp }}
                    </p>
                @endforeach
            </div>
            <div class="col-md-4 air">
                @foreach($sensorData as $data)
                    <p><i class="icon-cloud"></i>
                        {{ $data->qual }}
                    </p>
                @endforeach
            </div>
            <div class="col-md-4 energy">
                @foreach($sensorData as $data)
                    <p><i class="icon-flash"></i>
                        {{ $data->humi }}
                    </p>
                @endforeach
            </div>
        </div>
    </div>
    <div class="sub-container">
        <div class="col-md-4">
            <i class="icon-fire"></i>
            <div class='toggle' id='switch' onclick="getSensorData()">
                <div class='toggle-text-off'>OFF</div>
                <div class='glow-comp'></div>
                <div class='toggle-button'></div>
                <div class='toggle-text-on'>ON</div>
            </div>
            <i class="lock icon-lock"></i>
        </div>
        <div class="col-md-4">
            <i class="icon-th-large"></i>
            <div class='toggle' id='switch'>
                <div class='toggle-text-off'>OFF</div>
                <div class='glow-comp'></div>
                <div class='toggle-button'></div>
                <div class='toggle-text-on'>ON</div>
            </div>
            <i class="lock icon-lock"></i>
        </div>
        <div class="col-md-4">
            <i class="icon-lightbulb-o"></i>
            <div class='toggle' id='switch'>
                <div class='toggle-text-off'>OFF</div>
                <div class='glow-comp'></div>
                <div class='toggle-button'></div>
                <div class='toggle-text-on'>ON</div>
            </div>
            <i class="lock icon-lock"></i>
        </div>
    </div>
@endsection
